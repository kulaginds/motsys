<?php

use yii\db\Migration;
use app\models\RewardHistory;
use app\models\Employee;
use app\models\Reward;

/**
 * Class m180604_135503_create_table_rewards_history
 */
class m180604_135503_create_table_rewards_history extends Migration
{
    public function safeUp()
    {
        $this->createTable(RewardHistory::tableName(), [
            'employee_id' => $this->smallInteger(4)->notNull()->unsigned(),
            'reward_id' => $this->integer(10)->notNull()->unsigned(),
            'date' => $this->date()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addPrimaryKey('', RewardHistory::tableName(), [
            'employee_id',
            'reward_id',
            'date',
        ]);

        $this->addForeignKey('fk_rewards_history_employees', RewardHistory::tableName(),
            'employee_id', Employee::tableName(), 'id',
            'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_rewards_history_rewards', RewardHistory::tableName(),
            'reward_id', Reward::tableName(), 'id',
            'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m180604_135503_create_table_rewards_history cannot be reverted.\n";

        $this->dropForeignKey('fk_rewards_history_employees', RewardHistory::tableName());
        $this->dropForeignKey('fk_rewards_history_rewards', RewardHistory::tableName());
        $this->dropTable(RewardHistory::tableName());
    }
}
