<?php

use yii\db\Migration;
use app\models\Employee;

/**
 * Class m180601_193103_create_table_employees
 */
class m180601_193103_create_table_employees extends Migration
{
    public function safeUp()
    {
        $this->createTable(Employee::tableName(), [
            'id' => $this->primaryKey(4),
            'name' => $this->string(50)->notNull(),
            'birthday' => $this->date(),
            'phone' => $this->string(13),
            'address' => $this->string(100),
            'hire_date' => $this->date(),
            'dismissal_date' => $this->date(),
            'password_hash' => $this->string(60),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->alterColumn(Employee::tableName(), 'id',
            $this->smallInteger(4)->notNull()->unsigned() . ' AUTO_INCREMENT');
    }

    public function safeDown()
    {
        echo "m180601_193103_create_table_employees cannot be reverted.\n";

        $this->dropTable(Employee::tableName());
    }
}
