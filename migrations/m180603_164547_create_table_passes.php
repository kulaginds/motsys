<?php

use yii\db\Migration;
use app\models\Pass;
use app\models\Employee;

/**
 * Class m180603_164547_create_table_passes
 */
class m180603_164547_create_table_passes extends Migration
{
    public function safeUp()
    {
        $this->createTable(Pass::tableName(), [
            'employee_id' => $this->primaryKey(4),
            'bad_absence_count' => $this->integer(10)->notNull()->unsigned()
                ->defaultValue(0),
            'good_absence_count' => $this->integer(10)->notNull()->unsigned()
                ->defaultValue(0),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->alterColumn(Pass::tableName(), 'employee_id',
            $this->smallInteger(4)->notNull()->unsigned());

        $this->addForeignKey('fk_passes_employees', Pass::tableName(),
            'employee_id', Employee::tableName(), 'id',
            'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m180603_164547_create_table_passes cannot be reverted.\n";

        $this->dropForeignKey('fk_passes_employees', Pass::tableName());
        $this->dropTable(Pass::tableName());
    }
}
