<?php

use yii\db\Migration;
use app\models\Reward;

/**
 * Class m180604_135150_create_table_rewards
 */
class m180604_135150_create_table_rewards extends Migration
{
    public function safeUp()
    {
        $this->createTable(Reward::tableName(), [
            'id' => $this->primaryKey(10),
            'name' => $this->string(50)->notNull(),
            'cost' => $this->integer(10)->notNull()->unsigned()
                ->defaultValue(0),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->alterColumn(Reward::tableName(), 'id', $this->integer(10)
                ->notNull()->unsigned() . ' AUTO_INCREMENT');
    }

    public function safeDown()
    {
        echo "m180604_135150_create_table_rewards cannot be reverted.\n";

        $this->dropTable(Reward::tableName());
    }
}
