<?php

use yii\db\Migration;
use app\models\DonePart;
use app\models\Employee;

/**
 * Class m180603_171000_create_table_done_parts
 */
class m180603_171000_create_table_done_parts extends Migration
{
    public function safeUp()
    {
        $this->createTable(DonePart::tableName(), [
            'employee_id' => $this->smallInteger(4)->notNull()->unsigned(),
            'date' => $this->date()->notNull(),
            'count' => $this->integer(10)->notNull()->unsigned()
                ->defaultValue(0),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addPrimaryKey('', DonePart::tableName(), ['employee_id', 'date']);

        $this->addForeignKey('fk_done_parts_employees', DonePart::tableName(),
            'employee_id', Employee::tableName(), 'id',
            'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m180603_171000_create_table_done_parts cannot be reverted.\n";

        $this->dropForeignKey('fk_done_parts_employees', DonePart::tableName());
        $this->dropTable(DonePart::tableName());
    }
}
