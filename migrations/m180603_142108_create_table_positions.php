<?php

use yii\db\Migration;
use app\models\Position;

/**
 * Class m180603_142108_create_table_positions
 */
class m180603_142108_create_table_positions extends Migration
{
    public function safeUp()
    {
        $this->createTable(Position::tableName(), [
            'id' => $this->primaryKey(10),
            'name' => $this->string(50)->notNull(),
            'min_done_parts' => $this->integer(10)->notNull()->unsigned()->defaultValue(0),
            'min_done_hours' => $this->integer(10)->notNull()->unsigned()->defaultValue(0),
            'part_count' => $this->float()->notNull()->unsigned()->defaultValue(0.0),
            'begin_time' => $this->time()->notNull(),
            'end_time' => $this->time()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->alterColumn(Position::tableName(), 'id', $this->integer(10)
            ->notNull()->unsigned() . ' AUTO_INCREMENT');
    }

    public function safeDown()
    {
        echo "m180603_142108_create_table_positions cannot be reverted.\n";

        $this->dropTable(Position::tableName());
    }
}
