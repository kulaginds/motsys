<?php

use yii\db\Migration;
use app\models\Checkpoint;
use app\models\Employee;

/**
 * Class m180603_172238_create_table_checkpoint
 */
class m180603_172238_create_table_checkpoint extends Migration
{
    public function safeUp()
    {
        $this->createTable(Checkpoint::tableName(), [
            'employee_id' => $this->smallInteger(4)->notNull()->unsigned(),
            'date' => $this->date()->notNull(),
            'work_hours' => $this->tinyInteger(2)->notNull()->unsigned()
                ->defaultValue(0),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addPrimaryKey('', Checkpoint::tableName(), ['employee_id', 'date']);

        $this->addForeignKey('fk_checkpoint_employees', Checkpoint::tableName(),
            'employee_id', Employee::tableName(), 'id',
            'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m180603_172238_create_table_checkpoint cannot be reverted.\n";

        $this->dropForeignKey('fk_checkpoint_employees', Checkpoint::tableName());
        $this->dropTable(Checkpoint::tableName());
    }
}
