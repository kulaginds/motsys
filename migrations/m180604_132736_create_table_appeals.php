<?php

use yii\db\Migration;
use app\models\Appeal;

/**
 * Class m180604_132736_create_table_appeals
 */
class m180604_132736_create_table_appeals extends Migration
{
    public function safeUp()
    {
        $this->createTable(Appeal::tableName(), [
            'id' => $this->primaryKey(10),
            'from' => $this->string(50),
            'to' => $this->string(50)->notNull(),
            'date' => $this->date()->notNull(),
            'text' => $this->text()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->alterColumn(Appeal::tableName(), 'id', $this->integer(10)
                ->notNull()->unsigned() . ' AUTO_INCREMENT');
    }

    public function safeDown()
    {
        echo "m180604_132736_create_table_appeals cannot be reverted.\n";

        $this->dropTable(Appeal::tableName());
    }
}
