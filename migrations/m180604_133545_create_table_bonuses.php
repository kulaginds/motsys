<?php

use yii\db\Migration;
use app\models\Bonus;
use app\models\Employee;

/**
 * Class m180604_133545_create_table_bonuses
 */
class m180604_133545_create_table_bonuses extends Migration
{
    public function safeUp()
    {
        $this->createTable(Bonus::tableName(), [
            'employee_id' => $this->primaryKey(4),
            'count' => $this->integer(10)->notNull()->unsigned()->defaultValue(0),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->alterColumn(Bonus::tableName(), 'employee_id',
            $this->smallInteger(4)->notNull()->unsigned());

        $this->addForeignKey('fk_bonuses_employees', Bonus::tableName(),
            'employee_id', Employee::tableName(), 'id',
            'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m180604_133545_create_table_bonuses cannot be reverted.\n";

        $this->dropForeignKey('fk_bonuses_employees', Bonus::tableName());
        $this->dropTable(Bonus::tableName());
    }
}
