<?php

use yii\db\Migration;
use app\models\Manufactory;

/**
 * Class m180603_161657_create_table_manufactories
 */
class m180603_161657_create_table_manufactories extends Migration
{
    public function safeUp()
    {
        $this->createTable(Manufactory::tableName(), [
            'id' => $this->primaryKey(10),
            'name' => $this->string(50)->notNull(),
            'employee_count' => $this->integer(10)->notNull()->unsigned()
                ->defaultValue(0),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->alterColumn(Manufactory::tableName(), 'id', $this->integer(10)
            ->notNull()->unsigned() . ' AUTO_INCREMENT');
    }

    public function safeDown()
    {
        echo "m180603_161657_create_table_manufactories cannot be reverted.\n";

        $this->dropTable(Manufactory::tableName());
    }
}
