<?php

use yii\db\Migration;
use app\models\Employee;
use app\models\Position;
use app\models\Manufactory;

/**
 * Class m180603_163203_add_foreign_keys_to_table_employees
 */
class m180603_163203_add_foreign_keys_to_table_employees extends Migration
{
    public function safeUp()
    {
        $this->addColumn(Employee::tableName(), 'position_id', $this->integer(10)
            ->notNull()->unsigned());
        $this->addForeignKey('fk_employees_positions', Employee::tableName(),
            'position_id', Position::tableName(), 'id', 'CASCADE', 'CASCADE');

        $this->addColumn(Employee::tableName(), 'manufactory_id', $this->integer(10)
            ->notNull()->unsigned());
        $this->addForeignKey('fk_employees_manufactories', Employee::tableName(),
            'manufactory_id', Manufactory::tableName(), 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m180603_163203_add_foreign_keys_to_table_employees cannot be reverted.\n";

        $this->dropForeignKey('fk_employees_positions', Employee::tableName());
        $this->dropColumn(Employee::tableName(), 'position_id');

        $this->dropForeignKey('fk_employees_manufactories', Employee::tableName());
        $this->dropColumn(Employee::tableName(), 'manufactory_id');
    }
}
