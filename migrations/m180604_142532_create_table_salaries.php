<?php

use yii\db\Migration;
use app\models\Salary;
use app\models\Employee;

/**
 * Class m180604_142532_create_table_salaries
 */
class m180604_142532_create_table_salaries extends Migration
{
    public function safeUp()
    {
        $this->createTable(Salary::tableName(), [
            'employee_id' => $this->smallInteger(4)->notNull()->unsigned(),
            'month' => $this->char(7)->notNull(),
            'sum' => $this->integer(10)->notNull()->unsigned()->defaultValue(0),
            'award' => $this->float()->notNull()->unsigned()->defaultValue(0.0),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addPrimaryKey('', Salary::tableName(), ['employee_id', 'month']);

        $this->addForeignKey('fk_salaries_employees', Salary::tableName(),
            'employee_id', Employee::tableName(), 'id',
            'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m180604_142532_create_table_salaries cannot be reverted.\n";

        $this->dropForeignKey('fk_salaries_employees', Salary::tableName());
        $this->dropTable(Salary::tableName());
    }
}
