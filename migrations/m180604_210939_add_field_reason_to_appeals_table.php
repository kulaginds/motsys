<?php

use yii\db\Migration;
use app\models\Appeal;

/**
 * Class m180604_210939_add_field_reason_to_appeals_table
 */
class m180604_210939_add_field_reason_to_appeals_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn(Appeal::tableName(), 'reason', $this->string(50)
            ->notNull());
    }

    public function safeDown()
    {
        echo "m180604_210939_add_field_reason_to_appeals_table cannot be reverted.\n";

        $this->dropColumn(Appeal::tableName(), 'reason');
    }
}
