<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\RegForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
?>
<link rel="stylesheet" href="log_style.css">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <h2>Регистрация</h2>
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'number')->textInput(['autofocus' => true])->label('Табельный номер:') ?>

            <?= $form->field($model, 'password')->passwordInput()->label('Пароль:') ?>

            <?= $form->field($model, 'password_repeat')->passwordInput()->label('Повторите пароль еще раз:') ?>

            <?= Html::submitButton('Зарегистрироваться!', ['class' => 'btn btn-primary', 'name' => 'reg-button']) ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>