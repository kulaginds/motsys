<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Главная';
?>
<div class="employee_card" style="margin-top: 5vh;">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4">
                <h4>Добро пожаловать!</h4>
                <div class="card" style="width:280px; height:400px;">
                    <img class="card-img-top"src ="img/img_avatar1.png" alt="Card image" style="width:100%">
                    <div class="card-body">
                        <h5 class="card-title"><?=Yii::$app->user->identity->name?></h5>
                        <center>
                            <?=Html::beginForm(['/site/logout'], 'post')?>
                            <?=Html::submitButton(
                                'Выйти',
                                ['class' => 'btn btn-danger']
                            )?>
                            <?=Html::endForm()?>
                        </center>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <h4 style="margin-bottom: 15px;">Личная карта работника</h4>
                <div class="card_list">
                    <a href="#" class="list-group-item list-group-item-action"><h5>Табельный номер: <?=Yii::$app->user->identity->id?></h5></a>
                    <a href="#" class="list-group-item list-group-item-action"><h5>Должность: <?=Yii::$app->user->identity->getPosition()->name?></h5></a>
                    <a href="#" class="list-group-item list-group-item-action"><h5>Цех: <?=Yii::$app->user->identity->getManufactory()->name?></h5></a>
                    <a href="#" class="list-group-item list-group-item-action"><h5>Количество бонусов: <?=Yii::$app->user->identity->getBonus()->count?></h5></a>
                    <a href="#" class="list-group-item list-group-item-action"><h5>Cтаж работы: <?=Yii::$app->user->identity->getExpirience()?></h5></a>
                    <a href="#" class="list-group-item list-group-item-action"><h5>Дата рождения: <?=Yii::$app->user->identity->birthday?></h5></a>
                    <a href="#" class="list-group-item list-group-item-action"><h5>Рабочий график: <?=Yii::$app->user->identity->getSchedule()?></h5></a>
                </div>
            </div>
        </div>
    </div>
</div>
