<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Приветствие';
?>
<link rel="stylesheet" href="log_style.css">

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <center><h1>Добро пожаловать в BrestmashWebapp, пожалуйста, зарегистируйтесь или авторизуйтесь</h1></center>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-4">
            <a href ="<?=Url::toRoute(['/site/login']) ?>">  <button type="button" class="btn btn-info">Авторизация</button></a>
            <a href="<?=Url::toRoute(['/site/reg']) ?>"> <button type="button" class="btn btn-primary">Регистрация</button></a>
        </div>
    </div>
</div>