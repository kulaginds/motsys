<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Лучшие сотрудники';
?>
<div class="container">
    <h4>Лучшие работники месяца</h4>
    <div class ="row">

        <div class="col-md-3 ">

            <div class="card" style="width:270px; height:400px;">
                <img class="card-img-top"src ="img/img_avatar1.png" alt="Card image" style="width:100%; height:55%;">
                <div class="card-body">
                    <h5 class="card-title">Петров Петр Петрович</h5>
                    <p>Должность:</p>
                    <p>Цех:</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        Посмотреть профиль
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-3 ">

            <div class="card" style="width:270px; height:400px;">
                <img class="card-img-top"src ="img/img_avatar1.png" alt="Card image" style="width:100%; height:55%;">
                <div class="card-body">
                    <h5 class="card-title">Петров Петр Петрович</h5>
                    <p>Должность:</p>
                    <p>Цех:</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        Посмотреть профиль
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-3">

            <div class="card" style="width:270px; height:400px;">
                <img class="card-img-top"src ="img/img_avatar1.png" alt="Card image" style="width:100%; height:55%;">
                <div class="card-body">
                    <h5 class="card-title">Петров Петр Петрович</h5>
                    <p>Должность:</p>
                    <p>Цех:</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        Посмотреть профиль
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-3">

            <div class="card" style="width:270px; height:400px;">
                <img class="card-img-top"src ="img/img_avatar1.png" alt="Card image" style="width:100%; height:55%;">
                <div class="card-body">
                    <h5 class="card-title">Петров Петр Петрович</h5>
                    <p>Должность:</p>
                    <p>Цех:</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        Посмотреть профиль
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-3">

            <div class="card" style="width:270px; height:400px;">
                <img class="card-img-top"src ="img/img_avatar1.png" alt="Card image" style="width:100%; height:55%;">
                <div class="card-body">
                    <h5 class="card-title">Петров Петр Петрович</h5>
                    <p>Должность:</p>
                    <p>Цех:</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        Посмотреть профиль
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-3 ">

            <div class="card" style="width:270px; height:400px;">
                <img class="card-img-top"src ="img/img_avatar1.png" alt="Card image" style="width:100%; height:55%;">
                <div class="card-body">
                    <h5 class="card-title">Петров Петр Петрович</h5>
                    <p>Должность:</p>
                    <p>Цех:</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        Посмотреть профиль
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-3">

            <div class="card" style="width:270px; height:400px;">
                <img class="card-img-top"src ="img/img_avatar1.png" alt="Card image" style="width:100%; height:55%;">
                <div class="card-body">
                    <h5 class="card-title">Петров Петр Петрович</h5>
                    <p>Должность:</p>
                    <p>Цех:</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        Посмотреть профиль
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-3">

            <div class="card" style="width:270px; height:400px;">
                <img class="card-img-top"src ="img/img_avatar1.png" alt="Card image" style="width:100%; height:55%;">
                <div class="card-body">
                    <h5 class="card-title">Петров Петр Петрович</h5>
                    <p>Должность:</p>
                    <p>Цех:</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        Посмотреть профиль
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
