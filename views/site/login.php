<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Авторизация';
?>
<link rel="stylesheet" href="log_style.css">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-4">
            <h2>Авторизация</h2>
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'number')->textInput(['autofocus' => true])->label('Табельный номер:') ?>

            <?= $form->field($model, 'password')->passwordInput()->label('Пароль:') ?>

            <?= $form->field($model, 'rememberMe')->checkbox()->label('Запомнить меня') ?>

            <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>