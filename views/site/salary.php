<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Расчет зарплаты';
?>
<div class="salaries_result" style="margin-top: 5vh;">
    <div class="container">
        <div class="alert alert-info">
            <b><?=Yii::$app->user->identity->name?>!</b> По вашему запросу был произведен расчет зарплаты за текущий месяц.
        </div>

        <?php if (Yii::$app->user->identity->getSalary()->hasHoursRebuke()): ?>
        <div class="alert alert-danger">
            <b><?=Yii::$app->user->identity->name?>!</b> Вас ждёт выговор от начальства за неотработанные часы.
        </div>
        <?php endif; ?>

        <?php if (Yii::$app->user->identity->getSalary()->hasPassRebuke()): ?>
        <div class="alert alert-danger">
            <b><?=Yii::$app->user->identity->name?>!</b> Вас ждёт выговор от начальства за пропуск по неуважительной причине.
        </div>
        <?php endif; ?>

        <h3><?=Yii::$app->user->identity->getSalary()->getCalcMonth()?></h3>

        <h4>Ваши рабочие показатели на текущий момент составляют:</h4>
        <div class="list-group">
            <h5 class="list-group-item list-group-item-action">Отработанных часов: <?=Yii::$app->user->identity->getSalary()->getWorkHours()?></h5>
            <h5 class="list-group-item list-group-item-action">Сделаных деталей: <?=Yii::$app->user->identity->getSalary()->getDoneParts()?></h5>
            <h5 class="list-group-item list-group-item-action">Прогулов по ув. причине: <?=Yii::$app->user->identity->getPass()->good_absence_count?></h5>
            <h5 class="list-group-item list-group-item-action">Прогулов по неув. причине: <?=Yii::$app->user->identity->getPass()->bad_absence_count?></h5>
            <h5 class="list-group-item list-group-item-action">Ваша зарплата составит: <?=Yii::$app->user->identity->getSalary()->getSum()?></h5>
            <h5 class="list-group-item list-group-item-action">Ваша премия составит: <?=Yii::$app->user->identity->getSalary()->getAward()?></h5>
            <h5 class="list-group-item list-group-item-action">Итого: <?=Yii::$app->user->identity->getSalary()->getTotal()?></h5>
        </div>
        <div class="alert alert-dark" style="margin-top: 10px;">
            Расчет зарплаты был произведен согласно сдельно-премиальной оплате труда
        </div>
        <div class="alert alert-warning">
            <strong>По всем вопросам обращайтесь к начальству или в бухгалтерию</strong>
        </div>
        <br>
        <br>
    </div>
</div>
