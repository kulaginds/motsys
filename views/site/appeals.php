<?php

/* @var $this yii\web\View */
/* @var $model app\models\AppealForm */
/* @var $isSend bool */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Обращения к руководству';
?>
<div class="container">
    <br>
    <p><b><?=Yii::$app->user->identity->name?></b>, вы перешли на страницу обращения к руководству, здесь вы можете написать жалобу/просьбу или сообщить о какой-либо ситуации, произошедшей во время рабочего процесса.</p>
    <p>Вы можете оставить анонимное или персональное обращение.</p>

    <?php if ($isSend): ?>
    <div class="alert alert-success" style="margin-top: 5vh;">
        <strong>Обращение успешно отправлено!</strong>
    </div>
    <?php endif; ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'reason')->textInput(['autofocus' => true])->label($model->attributeLabels()['reason'] . ':') ?>

    <?= $form->field($model, 'to')->textInput()->label($model->attributeLabels()['to'] . ':') ?>

    <?= $form->field($model, 'text')->textarea()->label($model->attributeLabels()['text'] . ':') ?>

    <?= $form->field($model, 'isAnonymous')->label('')->inline()->radioList([1 => 'Анонимное', 0 => 'Персональное'], ['separator' => '&nbsp;&nbsp;&nbsp;']); ?>

    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>

    <?php ActiveForm::end(); ?>

    <br>
    <br>
    <br>
    <br>
</div>
