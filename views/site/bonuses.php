<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Система бонусов';
?>
<div class="container">
    <div class="alert alert-success" style="margin-top: 5vh;">
        <strong>Username, вы приветсвуем вас в системе бонусов на данный момент у вас имеется n баллов!  Выберете награду из списка.</strong>
    </div>
    <h5> Выберете награду из списка</h5>
    <h6> У вас на счету: N баллов!</h6>
    <div class = "row">
        <div class="col-md-3">
            <div class="card" style="width:270px; height:400px;">
                <img class="card-img-top"src ="img/3.jpg" alt="Card image" style="width:100%; height:55%;">
                <div class="card-body">
                    <h5 class="card-title">Выходной</h5>
                    <p>Cтоимость: N</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        Выбрать награду
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card" style="width:270px; height:400px;">
                <img class="card-img-top"src ="img/4.jpg" alt="Card image" style="width:100%; height:55%;">
                <div class="card-body">
                    <h5 class="card-title">Аванс</h5>
                    <p>Cтоимость: N</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        Выбрать награду
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card" style="width:270px; height:400px;">
                <img class="card-img-top"src ="img/2.jpg" alt="Card image" style="width:100%; height:55%;">
                <div class="card-body">
                    <h5 class="card-title">Внеочередной отпуск</h5>
                    <p>Cтоимость: N</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        Выбрать награду
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card" style="width:270px; height:400px;">
                <img class="card-img-top"src ="img/1.jpg" alt="Card image" style="width:100%; height:55%;">
                <div class="card-body">
                    <h5 class="card-title">Доп.премия к зарплате</h5>
                    <p>Cтоимость: N</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        Выбрать награду
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Ваша награда успешно выбрана!</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                Распечатайте пожалуйста данную выписку и по приходу на работу предьявите начальству.
            </div>
        </div>
    </div>
</div>
