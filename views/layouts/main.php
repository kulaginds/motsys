<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="style/style.css">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    </head>
    <body>
<?php $this->beginBody() ?>

<?php
NavBar::begin([
        'headerContent' => '<style>.navbar-toggle { display: none; }</style>',
    'options' => [
        'class' => 'navbar navbar-expand-lg bg-dark navbar-dark',
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => [
        ['label' => 'Главная', 'url' => ['/site/index'], 'options' => ['class' => 'nav-item'], 'linkOptions' => ['class' => 'nav-link']],
        ['label' => 'Расcчитать зараплату', 'url' => ['/site/salary'], 'options' => ['class' => 'nav-item'], 'linkOptions' => ['class' => 'nav-link']],
        ['label' => 'Обращение к руководству', 'url' => ['/site/appeals'], 'options' => ['class' => 'nav-item'], 'linkOptions' => ['class' => 'nav-link']],
        ['label' => 'Система бонусов', 'url' => ['/site/bonuses'], 'options' => ['class' => 'nav-item'], 'linkOptions' => ['class' => 'nav-link']],
        ['label' => 'Лучшие работники месяца', 'url' => ['/site/best'], 'options' => ['class' => 'nav-item'], 'linkOptions' => ['class' => 'nav-link']],
        ['label' => 'Инструкция', 'url' => ['/site/instructions'], 'options' => ['class' => 'nav-item'], 'linkOptions' => ['class' => 'nav-link']],
    ],
]);
NavBar::end();
?>

<?= $content ?>

<footer>
    <nav class="nav navbar-expand-lg bg-dark navbar-dark" style="position: fixed; bottom:0; width: 100%;">
        <div class="container">
            <ul class="navbar-nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link" href="#">По всем вопросам обращайтесь в САСУП</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://www.bmz.by/">Разработано специально для ОАО "Брестмаш"</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Телефон отдела кадров:  28-70-64</a>
                </li>
            </ul>
        </div>
    </nav>
</footer>

<?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>

