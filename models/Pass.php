<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;

class Pass extends ActiveRecord
{
    public static function tableName()
    {
        return '{{passes}}';
    }

    public function rules()
    {
        return [
            [['employee_id', 'bad_absence_count', 'good_absence_count'], 'required']
        ];
    }
}