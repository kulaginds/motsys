<?php

namespace app\models;

use Yii;
use yii\base\Model;

class AppealForm extends Model
{
    public $reason;
    public $to;
    public $text;
    public $isAnonymous;

    private $_attributeLabels;

    public function rules()
    {
        return [
            [['reason', 'to', 'text', 'isAnonymous'], 'required'],
            ['isAnonymous', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        if (empty($this->_attributeLabels)) {
            $this->_attributeLabels = [
                'reason' => 'Укажите причину обращения',
                'to' => 'Кому адресовано обращение',
                'text' => 'Введите текст обращения',
                'isAnonymous' => 'Аниномность',
            ];
        }
        return $this->_attributeLabels;
    }

    public function send()
    {
        if ($this->validate()) {
            $appeal = new Appeal();
            $appeal->reason = $this->reason;
            $appeal->to = $this->to;
            $appeal->text = $this->text;
            $appeal->date = date('Y-m-d');

            if (!$this->isAnonymous) {
                $appeal->from = Yii::$app->user->identity->name;
            }

            return $appeal->save();
        }

        return false;
    }
}