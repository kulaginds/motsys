<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;
use \yii\web\IdentityInterface;

class Employee extends ActiveRecord implements IdentityInterface
{
    /** @var $id */
    /** @var $name */
    /** @var $birthday */
    /** @var $phone */
    /** @var $address */
    /** @var $hire_date */
    /** @var $dismissal_date */
    /** @var $password_hash */

    private $_position;
    private $_manufactory;
    private $_bonus;
    private $_salary;
    private $_pass;

    public static function tableName()
    {
        return '{{employees}}';
    }

    public function rules()
    {
        return [
            [['id', 'name'], 'required']
        ];
    }

    public function getPosition()
    {
        if (empty($this->_position)) {
            $this->_position = Position::find()
                ->where(['id' => $this->position_id])
                ->one();
        }

        return $this->_position;
    }

    public function getManufactory()
    {
        if (empty($this->_manufactory)) {
            $this->_manufactory = Manufactory::find()
                ->where(['id' => $this->manufactory_id])
                ->one();
        }

        return $this->_manufactory;
    }

    public function getBonus()
    {
        if (empty($this->_bonus)) {
            $this->_bonus = Bonus::find()
                ->where(['employee_id' => $this->id])
                ->one();

            if (empty($this->_bonus)) {
                $this->_bonus = new Bonus();
            }
        }

        return $this->_bonus;
    }

    public function getExpirience()
    {
        $now = new \DateTime();
        $hire = new \DateTime($this->hire_date);
        $interval = $now->diff($hire);

        return $interval->y . ' лет, ' . $interval->m . ' месяцев';
    }

    public function getSchedule()
    {
        $position = $this->getPosition();

        return $position->begin_time . ' - ' . $position->end_time;
    }

    public function getSalary()
    {
        if (empty($this->_salary)) {
            $this->_salary = Salary::find()
                ->where(['employee_id' => $this->id])
                ->orderBy('month DESC')
                ->one();

            if (empty($this->_salary)) {
                $this->_salary = new Salary();
                $this->_salary->employee_id = $this->id;
                $this->_salary->month = date('Y-m');
            } else {
                $this->_salary->switchNextMonth();
            }
        }

        return $this->_salary;
    }

    public function getPass()
    {
        if (empty($this->_pass)) {
            $this->_pass = Pass::find()
                ->where(['employee_id' => $this->id])
                ->one();

            if (empty($this->_pass)) {
                $this->_pass = new Pass();
                $this->_pass->employee_id = $this->id;
                $this->save();
            }
        }

        return $this->_pass;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne($username);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return false;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
    }
}
