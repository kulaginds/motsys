<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;

class Position extends ActiveRecord
{
    public static function tableName()
    {
        return '{{positions}}';
    }

    public function rules()
    {
        return [
            [['id', 'name', 'min_done_parts', 'min_done_hours', 'part_count', 'begin_time', 'end_time'], 'required']
        ];
    }
}