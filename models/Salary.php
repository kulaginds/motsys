<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;

class Salary extends ActiveRecord
{
    private $_work_hours;
    private $_done_parts;
    private $_sum;
    private $_award;
    private $_hasHoursRebuke;
    private $_hasPassRebuke;

    public static function tableName()
    {
        return '{{salaries}}';
    }

    public function rules()
    {
        return [
            [['employee_id', 'month', 'sum', 'award'], 'required']
        ];
    }

    public function switchNextMonth()
    {
        $this->month = date('Y-m', strtotime($this->month) + 31 * 24 * 60 * 60);
        $this->sum = 0;
        $this->award = 0.0;
    }

    public function getWorkHours()
    {
        if (empty($this->_work_hours)) {
            $this->_work_hours = Checkpoint::find()
                ->where(['like', '{{date}}', $this->month])
                ->andWhere(['employee_id' => $this->employee_id])
                ->sum('work_hours');

            if (empty($this->_work_hours)) {
                $this->_work_hours = 0;
            }
        }

        return $this->_work_hours;
    }

    public function getDoneParts()
    {
        if (empty($this->_done_parts)) {
            $this->_done_parts = DonePart::find()
                ->where(['like', '{{date}}', $this->month])
                ->andWhere(['employee_id' => $this->employee_id])
                ->sum('{{count}}');

            if (empty($this->_done_parts)) {
                $this->_done_parts = 0;
            }
        }

        return $this->_done_parts;
    }

    public function getSum()
    {
        if (empty($this->_sum)) {
            /** @var Position $position */
            $position = Yii::$app->user->identity->getPosition();
            $this->_sum = $this->getDoneParts() * $position->part_count;
        }

        return $this->_sum;
    }

    public function getAward()
    {
        if (empty($this->_award)) {
            /** @var Position $position */
            $position = Yii::$app->user->identity->getPosition();
            $this->_award = 0;
            $diff = $this->getDoneParts() - $position->min_done_parts;

            if ($diff > 0) {
                $this->_award = $diff * $position->part_count * 1.15;
            }
        }

        return $this->_award;
    }

    public function getTotal()
    {
        return $this->getSum() + $this->getAward();
    }

    public function hasHoursRebuke()
    {
        if (empty($this->_hasHoursRebuke)) {
            /** @var Position $position */
            $position = Yii::$app->user->identity->getPosition();
            $parts_diff = $this->getDoneParts() - $position->min_done_parts;
            $hours_diff = $this->getWorkHours() - $position->min_done_hours;

            $this->_hasHoursRebuke = (($parts_diff < 0) AND ($hours_diff < 0));
        }

        return $this->_hasHoursRebuke;
    }

    public function hasPassRebuke()
    {
        if (empty($this->_hasPassRebuke)) {
            /** @var Pass $pass */
            $pass = Yii::$app->user->identity->getPass();

            $this->_hasPassRebuke = $pass->bad_absence_count >= 2;
        }

        return $this->_hasPassRebuke;
    }

    public function getCalcMonth()
    {
        $time = strtotime($this->month);
        $month = date('n', $time);

        switch ($month) {
            case 1:
                $month = Yii::t('app', 'January', [], Yii::$app->language);
                break;
            case 2:
                $month = Yii::t('app', 'February', [], Yii::$app->language);
                break;
            case 3:
                $month = Yii::t('app', 'March', [], Yii::$app->language);
                break;
            case 4:
                $month = Yii::t('app', 'April', [], Yii::$app->language);
                break;
            case 5:
                $month = Yii::t('app', 'May', [], Yii::$app->language);
                break;
            case 6:
                $month = Yii::t('app', 'June', [], Yii::$app->language);
                break;
            case 7:
                $month = Yii::t('app', 'July', [], Yii::$app->language);
                break;
            case 8:
                $month = Yii::t('app', 'August', [], Yii::$app->language);
                break;
            case 9:
                $month = Yii::t('app', 'September', [], Yii::$app->language);
                break;
            case 10:
                $month = Yii::t('app', 'October', [], Yii::$app->language);
                break;
            case 11:
                $month = Yii::t('app', 'November', [], Yii::$app->language);
                break;
            case 12:
                $month = Yii::t('app', 'December', [], Yii::$app->language);
                break;
        }

        return date('Y', $time) . ', ' . $month;
    }
}