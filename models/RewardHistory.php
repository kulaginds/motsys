<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;

class RewardHistory extends ActiveRecord
{
    public static function tableName()
    {
        return '{{rewards_history}}';
    }

    public function rules()
    {
        return [
            [['employee_id', 'reward_id', 'date'], 'required']
        ];
    }
}