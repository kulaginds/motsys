<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;

class Manufactory extends ActiveRecord
{
    public static function tableName()
    {
        return '{{manufactories}}';
    }

    public function rules()
    {
        return [
            [['id', 'name', 'employee_count'], 'required']
        ];
    }
}