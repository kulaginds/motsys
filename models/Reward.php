<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;

class Reward extends ActiveRecord
{
    public static function tableName()
    {
        return '{{rewards}}';
    }

    public function rules()
    {
        return [
            [['id', 'name', 'cost'], 'required']
        ];
    }
}