<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;

class Checkpoint extends ActiveRecord
{
    public static function tableName()
    {
        return '{{checkpoint}}';
    }

    public function rules()
    {
        return [
            [['employee_id', 'date', 'work_hours'], 'required']
        ];
    }
}