<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;

class Appeal extends ActiveRecord
{
    public static function tableName()
    {
        return '{{appeals}}';
    }

    public function rules()
    {
        return [
            [['reason', 'to', 'date', 'text'], 'required']
        ];
    }
}