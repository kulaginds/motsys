<?php

namespace app\models;

use yii\base\Model;

class RegForm extends Model
{
    public $number;
    public $password;
    public $password_repeat;

    private $_employee = false;

    public function rules()
    {
        return [
            [['number', 'password', 'password_repeat'], 'required'],
            ['password_repeat', 'validatePasswordRepeat'],
            ['number', 'validateNumber'],
        ];
    }

    public function validatePasswordRepeat($attribute, $params)
    {
        if ($this->hasErrors()) {
            return;
        }

        if ($this->password != $this->password_repeat) {
            $this->addError($attribute, 'Password repeat not equal with password');
        }
    }

    public function validateNumber($attribute, $params)
    {
        if ($this->hasErrors()) {
            return;
        }

        $employee = $this->getEmployee();

        if (is_null($employee)) {
            $this->addError($attribute, 'Employee not exists');
        }
    }

    /**
     * Signs user up.
     *
     * @return Employee|null the saved model or null if saving fails
     */
    public function reg()
    {
        if (!$this->validate()) {
            return null;
        }

        $employee = $this->getEmployee();
        $employee->setPassword($this->password);

        return $employee->save() ? $employee : null;
    }

    public function getEmployee()
    {
        if ($this->_employee === false) {
            $this->_employee = Employee::findIdentity($this->number);
        }

        return $this->_employee;
    }
}
