<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;

class DonePart extends ActiveRecord
{
    public static function tableName()
    {
        return '{{done_parts}}';
    }

    public function rules()
    {
        return [
            [['employee_id', 'date', 'count'], 'required']
        ];
    }
}