<?php

namespace app\controllers;

use app\models\AppealForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\RegForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'salary', 'appeals', 'bonuses', 'instructions', 'best'],
                'rules' => [
                    [
                        'actions' => ['logout', 'salary', 'appeals', 'bonuses', 'instructions', 'best'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            $this->layout = 'anonymous';
            return $this->render('index');
        } else {
            return $this->render('main');
        }
    }

    /**
     * Reg action.
     *
     * @return Response|string
     */
    public function actionReg()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'anonymous';

        $model = new RegForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->reg()) {
                if (Yii::$app->user->login($user)) {
                    return $this->goHome();
                }
            }
        }

        $model->password = '';
        $model->password_repeat = '';
        return $this->render('reg', [
            'model' => $model,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'anonymous';

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSalary()
    {
        return $this->render('salary');
    }

    public function actionAppeals()
    {
        $model = new AppealForm();
        $isSend = false;

        if ($model->load(Yii::$app->request->post()) && $model->send()) {
            $isSend = true;
            $model->reason = '';
            $model->to = '';
            $model->text = '';
            $model->isAnonymous = '';
        }

        return $this->render('appeals', [
            'model' => $model,
            'isSend' => $isSend,
        ]);
    }

    public function actionBonuses()
    {
        return $this->render('bonuses');
    }

    public function actionInstructions()
    {
        return $this->render('instructions');
    }

    public function actionBest()
    {
        return $this->render('best');
    }
}
